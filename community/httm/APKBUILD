# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=httm
pkgver=0.32.1
pkgrel=0
pkgdesc="Interactive, file-level Time Machine-like tool for ZFS/btrfs"
url="https://github.com/kimono-koans/httm"
# riscv64: TODO
# s390x: fails to build nix crate
arch="all !riscv64 !s390x"
license="MPL-2.0"
makedepends="cargo acl-dev cargo-auditable"
subpackages="$pkgname-doc"
source="https://github.com/kimono-koans/httm/archive/refs/tags/$pkgver/httm-$pkgver.tar.gz"
options="net !check"  # no tests provided

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release --features acls
}

package() {
	install -D -m755 target/release/$pkgname -t "$pkgdir"/usr/bin/
	install -D -m644 $pkgname.1 -t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
d50cd48fe03f77db75271261f23fe91f83bc0b5ce8d7c369e28928d9324d3bee28586bafbd3bcfacdf547a7a075a8ce8e9bf6a1851fb0e258328394e8b1f8f71  httm-0.32.1.tar.gz
"
