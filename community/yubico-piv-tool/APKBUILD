# Contributor: Christophe BERAUD-DUFOUR <christophe.berauddufour@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=yubico-piv-tool
pkgver=2.4.0
pkgrel=0
pkgdesc="PIV Tools for yubikey"
url="https://developers.yubico.com/yubico-piv-tool"
arch="all"
license="BSD-2-Clause"
makedepends="
	check-dev
	cmake
	gengetopt-dev
	help2man
	openssl-dev
	pcsc-lite-dev
	samurai
	zlib-dev
	"
subpackages="$pkgname-static $pkgname-dev $pkgname-libs $pkgname-doc"
source="https://developers.yubico.com/yubico-piv-tool/Releases/yubico-piv-tool-$pkgver.tar.gz
	werror.patch
	"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr

	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9684317138cdd0146a1ad03569b8f2b33262da87ce67196df8e0d3676e9fb7337266a77a8d155c128f560328237cc5c36f0726ee73ca9cff387e8c6d0ee5400b  yubico-piv-tool-2.4.0.tar.gz
1475032b9588bb56026a9850a041e7e287502fc53a7efe038c11ea60d719c166199f990a2760ff18b31c57be287825553de76dc79faf59e9d4064bfa8c01b31f  werror.patch
"
